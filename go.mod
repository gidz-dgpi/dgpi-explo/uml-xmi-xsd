module gitlab.com/istd-dgpi/dgpi-explo/uml-xmi

go 1.21.7

require (
	gitlab.com/istd-shared/go-hugo/book-theme v1.0.2 // indirect
	gitlab.com/istd-shared/go-hugo/kroki v1.0.2 // indirect
)
