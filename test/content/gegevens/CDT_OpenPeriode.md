---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Aanduiding van een tijdsperiode met een Begindatum en/of een Einddatum.
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De begindatum van de periode.
  dataType: LDT_Datum
  naam: Begindatum
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De einddatum van de periode.
  dataType: LDT_Datum
  naam: Einddatum
  verplicht: false
naam: CDT_OpenPeriode
regels:
- RS032
- CS003

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

