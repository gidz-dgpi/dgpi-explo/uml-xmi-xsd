---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De door de gemeente vastgestelde naam van een woonplaats.
maxLengte: '80'
minLengte: '1'
naam: LDT_Plaatsnaam
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

