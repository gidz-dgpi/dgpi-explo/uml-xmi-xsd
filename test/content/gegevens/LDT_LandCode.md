---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Identificatie van een (historisch/actueel) land of gebiedsdeel.
maxLengte: '2'
naam: LDT_LandCode

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

