---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Naam of nummer, die ter identificatie aan een totale aanlevering kan worden meegegeven.
maxLengte: '12'
minLengte: '1'
naam: LDT_IdentificatieBericht
regels:
- RS017
- RS033
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

