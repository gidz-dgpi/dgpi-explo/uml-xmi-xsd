---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Een door de overheid toegekend identificerend nummer in het kader van het vereenvoudigen
  van het contact tussen overheid en burgers en het verminderen van de administratieve
  lasten.
maxLengte: '9'
minLengte: '1'
naam: LDT_BurgerServicenummer
waarde: '[0-9]{9}'

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

