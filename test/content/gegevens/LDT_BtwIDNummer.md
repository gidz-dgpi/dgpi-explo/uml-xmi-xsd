---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- 'Het BTW-identificatienummer van een Nederlandse ondernemer bestaat uit veertien
  posities. Het begint met de landcode NL, gevolgd door negen cijfers, waarin het
  omzetbelastingnummer is opgenomen plus een toevoeging van drie posities: B01 t/m
  B99.'
- Het omzetbelastingnummer heeft vaak minder dan negen cijfers voor de toevoeging
  B01 t/m B99. In dat geval wordt het omzetbelastingnummer aangevuld met voorloopnullen.
  Er staan geen punten in het nummer.
maxLengte: '14'
minLengte: '1'
naam: LDT_BtwIDNummer
waarde: NL[0-9]{9}B[0-9]{2}

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

