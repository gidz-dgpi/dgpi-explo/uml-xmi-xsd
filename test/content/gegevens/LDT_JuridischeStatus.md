---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Juridische situatie die voor de client van toepassing is op het moment van beoordeling.
codeLijst: WJ232
maxLengte: '2'
naam: LDT_JuridischeStatus

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

