---
# python/object:istd_klassen.IstdDataType
beschrijving:
- De telefoonnummers waarop de client of relatie van de client te bereiken is.
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het telefoonnummer waarop de client of relatie van de client te bereiken is.
  dataType: CDT_Telefoon
  naam: Telefoon01
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het telefoonnummer waarop de client of relatie van de client te bereiken is.
  dataType: CDT_Telefoon
  naam: Telefoon02
  verplicht: false
naam: CDT_Telefoonnummers
regels:
- CS005
- RS018
- RS011
- RS033

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

