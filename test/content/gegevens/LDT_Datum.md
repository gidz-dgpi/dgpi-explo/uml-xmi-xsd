---
# python/object:istd_klassen.IstdDataType
basisType: date
beschrijving:
- Datum
naam: LDT_Datum
regels:
- RS032
waarde: '[^:Z]*'

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

