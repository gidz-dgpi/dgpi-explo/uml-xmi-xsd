---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Codering voor ja of nee.
codeLijst: COD260
maxLengte: '1'
naam: LDT_JaNee

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

