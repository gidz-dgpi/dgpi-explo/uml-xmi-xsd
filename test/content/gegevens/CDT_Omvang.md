---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Aanduiding van de omvang van de te leveren of geleverde ondersteuning, uitgedrukt
  in Volume, Eenheid en Frequentie.
codeLijst: WMO757
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Aanduiding van de mate van zorg betreffende een product.
  dataType: LDT_Volume
  naam: Volume
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Aanduiding van de eenheid waarin het volume is uitgedrukt.
  dataType: LDT_Eenheid
  naam: Eenheid
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Aanduiding van de frequentie waarmee een ondersteuningsproduct geleverd wordt.
  dataType: LDT_Frequentie
  naam: Frequentie
naam: CDT_Omvang
regels:
- RS001
- RS005
- CS330

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

