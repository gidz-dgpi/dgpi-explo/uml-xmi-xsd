---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De verzameling van letters die wordt gevormd door de eerste letter van alle in volgorde
  voorkomende voornamen van een persoon.
maxLengte: '6'
minLengte: '1'
naam: LDT_Voorletters
waarde: "([a-zA-Z\xC0-\u1EF3])+"

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

