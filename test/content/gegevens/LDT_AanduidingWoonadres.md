---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- De aanduiding die wordt gebruikt voor adressen die niet zijn voorzien van de gebruikelijke
  straatnaam en huisnummeraanduidingen.
maxLengte: '2'
naam: LDT_AanduidingWoonadres

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

