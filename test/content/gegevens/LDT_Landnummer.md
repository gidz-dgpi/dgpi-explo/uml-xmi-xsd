---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Het telefoonnummer van een land, vanuit Nederland benaderd.
maxLengte: '4'
minLengte: '1'
naam: LDT_Landnummer
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

