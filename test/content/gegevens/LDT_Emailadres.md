---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Emailadres van een client of relatie.
maxLengte: '80'
minLengte: '1'
naam: LDT_Emailadres
regels:
- RS026
- RS033
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

