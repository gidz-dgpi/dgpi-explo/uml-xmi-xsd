---
# python/object:istd_klassen.IstdDataType
basisType: integer
beschrijving:
- Nummer
maxWaarde: '999999999'
minWaarde: '0'
naam: LDT_Nummer
regels:
- RS001
- RS006

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

