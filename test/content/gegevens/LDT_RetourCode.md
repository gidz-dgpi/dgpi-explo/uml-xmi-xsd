---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Melding in een iStandaarden-retourbericht. Het betreft een code die in een retourbericht
  het resultaat van de beoordeling van een (deel van een) ontvangen bericht weergeeft.
codeLijst: WJ001
maxLengte: '4'
naam: LDT_RetourCode

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

