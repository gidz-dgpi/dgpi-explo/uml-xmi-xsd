---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Retourgegevens op hoofdniveau
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Uniek nummer waaronder de gemeente een declaratiebestand van een Wmo-aanbieder
    in zijn systeem heeft geregistreerd.
  dataType: LDT_GemeenteReferentieNummer
  naam: GemeenteReferentieNummer
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Datum van verzending van het retourbericht.
  dataType: LDT_Datum
  naam: RetourDagtekening
naam: CDT_RetourHeader
regels:
- RS020
- CS064
- CS324
- RS032

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

