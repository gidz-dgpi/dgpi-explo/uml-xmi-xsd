---
# python/object:istd_klassen.IstdDataType
basisType: integer
beschrijving:
- Aanduiding van de prioriteit van de contactpersoon.
maxWaarde: '99'
minWaarde: '0'
naam: LDT_RelatieVolgorde
regels:
- RS001
- RS002

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

