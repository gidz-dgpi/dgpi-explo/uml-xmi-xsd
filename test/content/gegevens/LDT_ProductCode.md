---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Gecodeerde aanduiding van een product voor zorg of ondersteuning.
maxLengte: '5'
minLengte: '1'
naam: LDT_ProductCode
regels:
- RS012
- RS033
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

