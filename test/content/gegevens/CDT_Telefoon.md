---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Het telefoonnummer waarop de client of relatie van de client te bereiken is.
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het telefoonnummer waarop de client of relatie van de client te bereiken is.
  dataType: LDT_Telefoonnummer
  naam: Telefoonnummer
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het telefoonnummer van een land, vanuit Nederland benaderd.
  dataType: LDT_Landnummer
  naam: Landnummer
naam: CDT_Telefoon

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

