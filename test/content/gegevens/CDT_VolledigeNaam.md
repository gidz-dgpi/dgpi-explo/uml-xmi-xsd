---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Volledige naam van een natuurlijk persoon, aangeduid als Geslachtsnaam, eventueel
  Partnernaam, Voornamen en/of Voorletters en NaamGebruik.
codeLijst: COD700
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Wettelijk vastgelegde achternaam die iemand bij de geboorte erft van de vader
    of moeder.
  dataType: CDT_Achternaam
  naam: Geslachtsnaam
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Na het voltrekken van een huwelijk of na een officiele partnerregistratie van
    de partner aangenomen familienaam.
  dataType: CDT_Achternaam
  naam: Partnernaam
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De verzameling namen die, gescheiden door spaties, aan de geslachtsnaam voorafgaat.
  dataType: LDT_Naam
  naam: Voornamen
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De verzameling van letters die wordt gevormd door de eerste letter van alle in
    volgorde voorkomende voornamen van een persoon.
  dataType: LDT_Voorletters
  naam: Voorletters
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Aanduiding naamgebruik (gecodeerd).
  dataType: LDT_NaamGebruik
  naam: NaamGebruik
naam: CDT_VolledigeNaam
regels:
- RS028
- RS033
- RS016
- CS004
- RS013
- CS050

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

