---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Het telefoonnummer waarop de client of relatie van de client te bereiken is.
maxLengte: '15'
minLengte: '1'
naam: LDT_Telefoonnummer
waarde: '[0-9]*'

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

