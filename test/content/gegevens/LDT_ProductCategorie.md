---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Gecodeerde aanduiding van hoofdcategorieen van producten in het kader van de Wet
  maatschappelijke ondersteuning (Wmo).
codeLijst: WMO020
maxLengte: '2'
naam: LDT_ProductCategorie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

