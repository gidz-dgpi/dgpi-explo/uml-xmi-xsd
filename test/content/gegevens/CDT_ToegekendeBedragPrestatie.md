---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Toegekend bedrag van het ingediende bedrag.
codeLijst: COD043
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Dit is het bedrag dat volgens de gemeente geoorloofd is te declareren.
  dataType: CDT_BedragMetDC
  naam: GemeenteBerekendBedrag
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het bedrag dat de betaler toekent aan de declarant.
  dataType: CDT_BedragMetDC
  naam: ToegekendBedrag
naam: CDT_ToegekendeBedragPrestatie
regels:
- IV024
- IV057
- RS001
- RS005
- CS325

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

