---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Code ter identificatie van een soort bericht. Per bericht is gespecificeerd welke
  code gebruikt moet worden. De volledige codelijst is te vinden op het portaal van
  Vektis (http://ei.vektis.nl/WespCodelijstenDetail.aspx?Co_Ge_Code=COD002&Co_Or_Code=VEKT).
maxLengte: '3'
minLengte: '1'
naam: LDT_BerichtCode
regels:
- CS126
- RS010

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

