---
# python/object:istd_klassen.IstdDataType
basisType: integer
beschrijving:
- Volgnummer van de formele uitgifte van een major release van een iStandaard.
maxWaarde: '99'
minWaarde: '0'
naam: LDT_BerichtVersie
regels:
- RS001
- RS002

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

