---
# python/object:istd_klassen.IstdDataType
beschrijving:
- Gegevens over de persoon of instantie die een client heeft doorverwezen naar ondersteuning,
  aangeduid als Type en Naam of ZorgverlenerCode.
codeLijst: COD327
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Gecodeerde aanduiding van het type verwijzer.
  dataType: LDT_TypeVerwijzer
  naam: Type
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Naam van de verwijzende persoon of instantie.
  dataType: LDT_NaamVerwijzer
  naam: Naam
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De AGBcode van de zorgverlener.
  dataType: LDT_AgbCode
  naam: ZorgverlenerCode
  verplicht: false
naam: CDT_Verwijzer
regels:
- CD066
- CD067
- RS025
- RS033
- CD068
- RS014
- RS036

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

