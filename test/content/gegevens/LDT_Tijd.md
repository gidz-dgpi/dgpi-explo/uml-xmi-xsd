---
# python/object:istd_klassen.IstdDataType
basisType: time
beschrijving:
- Tijd
naam: LDT_Tijd
regels:
- RS034
waarde: '[^Z+-]+'

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

