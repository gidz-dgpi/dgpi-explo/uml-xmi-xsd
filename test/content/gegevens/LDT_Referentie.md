---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Naam of nummer dat als referentie kan worden meegegeven.
maxLengte: '36'
minLengte: '1'
naam: LDT_Referentie
regels:
- RS024
- RS033
waarde: .*[^\s].*

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

