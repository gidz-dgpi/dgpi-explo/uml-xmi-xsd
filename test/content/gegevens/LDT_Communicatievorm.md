---
# python/object:istd_klassen.IstdDataType
basisType: string
beschrijving:
- Gecodeerde aanduiding van de bijzondere vorm van communicatie die gebruikt dient
  te worden.
maxLengte: '1'
naam: LDT_Communicatievorm

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

