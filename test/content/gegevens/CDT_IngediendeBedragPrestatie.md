---
# python/object:istd_klassen.IstdDataType
codeLijst: COD043
elementen:
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Hoeveelheid verricht product in een prestatieperiode. Altijd vullen in relatie
    met Eenheid.
  dataType: LDT_Volume
  naam: GeleverdVolume
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - De eenheid waarin de zorg/behandeling wordt uitgedrukt. Wordt gevuld met een Eenheid
    die hoort bij het GeleverdVolume.
  dataType: LDT_Eenheid
  naam: Eenheid
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Berekeningsfactor over het normtarief voor het feitelijk gebruikte aandeel (event.
    in tijd) aan te geven in de declaratie van kosten, honorarium of verrichting.
  dataType: LDT_Percentage
  naam: Verrekenpercentage
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Tarief van een individueel product.
  dataType: LDT_Bedrag
  naam: ProductTarief
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het berekend bedrag is het bedrag dat de zorgverlener wil ontvangen. Berekend
    bedrag wordt alleen gevuld indien gemeente en aanbieder dit hebben vastgelegd
    in onderlinge afspraken.
  dataType: CDT_BedragMetDC
  naam: BerekendBedrag
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Indicatie die aangeeft of sprake is van BTW-vrijstelling.
  dataType: LDT_BtwVrijstellingIndicatie
  naam: BtwVrijstellingIndicatie
  verplicht: false
- # python/object:istd_klassen.IstdElement
  dataType: LDT_Percentage
  maxWaarde: '9999'
  naam: BtwPercentage
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het bedrag aan BTW dat in rekening wordt gebracht.
  dataType: CDT_BedragMetDC
  naam: BtwBedrag
  verplicht: false
- # python/object:istd_klassen.IstdElement
  beschrijving:
  - Het bedrag dat gedeclareerd of gefactureerd wordt.
  dataType: CDT_BedragMetDC
  naam: DeclaratieFactuurBedrag
naam: CDT_IngediendeBedragPrestatie
regels:
- TR321
- TR322
- RS001
- RS005
- CS331
- TR309
- IV060
- IV068
- RS004
- CD062
- CS327
- TR317
- IV024
- IV057
- CS325
- CD059
- CD055
- CD056
- CS322
- CS323

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

