---
# python/object:istd_klassen.IstdCodeLijst
beschrijving: Reden afwijzing verzoek
codeItems:
- # python/object:istd_klassen.IstdCodeItem
  code: '1'
  documentatie: Geen wijziging, maar nieuwe aanvraag
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
- # python/object:istd_klassen.IstdCodeItem
  code: '2'
  documentatie: Past niet binnen budget
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
- # python/object:istd_klassen.IstdCodeItem
  code: '3'
  documentatie: Geen contract
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
- # python/object:istd_klassen.IstdCodeItem
  code: '4'
  documentatie: Past niet binnen contract
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
- # python/object:istd_klassen.IstdCodeItem
  code: '5'
  documentatie: Andere gemeente
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
- # python/object:istd_klassen.IstdCodeItem
  code: '6'
  documentatie: Stapeling
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
- # python/object:istd_klassen.IstdCodeItem
  code: '7'
  documentatie: Zorginhoudelijke afkeur
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
- # python/object:istd_klassen.IstdCodeItem
  code: '8'
  documentatie: Woonplaatsbeginsel
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
- # python/object:istd_klassen.IstdCodeItem
  code: '9'
  documentatie: Leeftijdsgrens is bereikt
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
documentatie: Gecodeerde aanduiding voor de reden van afwijzing van een verzoek.
naam: WJ759

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

