---
# python/object:istd_klassen.IstdCodeLijst
beschrijving: Reden wijziging toewijzing
codeItems:
- # python/object:istd_klassen.IstdCodeItem
  code: '01'
  documentatie: Administratieve correctie
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '02'
  documentatie: Client overleden
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '03'
  documentatie: Contractwijziging
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '04'
  documentatie: 'Herbeoordeling: verlenging toewijzing'
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '05'
  documentatie: 'Herbeoordeling: verkorting toewijzing'
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '06'
  documentatie: Uitstroom naar ander domein
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '07'
  documentatie: Verhuizing naar een andere gemeente
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: 08
  documentatie: Wijziging leveringsvorm
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: 09
  documentatie: Overstap naar andere aanbieder
  mutatie: niet gespecificeerd
- # python/object:istd_klassen.IstdCodeItem
  code: '10'
  documentatie: Overgang naar nieuwe bekostigingssystematiek
  ingangsdatum: 01-04-2018
  mutatie: toegevoegd
  mutatiedatum: 01-07-2017
- # python/object:istd_klassen.IstdCodeItem
  code: '11'
  documentatie: Gemeentelijke herindeling
  ingangsdatum: 01-04-2018
  mutatie: toegevoegd
  mutatiedatum: 01-07-2017
- # python/object:istd_klassen.IstdCodeItem
  code: '12'
  documentatie: Geinitieerd door de aanbieder
  ingangsdatum: 01-01-2021
  mutatie: toegevoegd
  mutatiedatum: 01-04-2020
documentatie: De reden waarom een toewijzing wordt gewijzigd.
naam: WMO002

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

