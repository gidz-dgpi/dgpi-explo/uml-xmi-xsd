---
# python/object:istd_klassen.IstdRegel
code: CD060
retourcode: '0001'
titel: 'CD060: Verplicht vullen indien DebetCredit bij het ingediende bedrag de waarde
  C (credit) heeft, anders leeg laten.'
type: Conditie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

