---
# python/object:istd_klassen.IstdRegel
code: IV055
documentatie: "Debet en credit prestaties kunnen in \xE9\xE9n declaratie- of factuurbericht\
  \ worden aangeleverd. Hierbij geldt:\nEen 1e debet prestatie en een identieke credit\
  \ prestatie mogen niet in \xE9\xE9n bericht (declaratie/factuur) worden aangeleverd.\
  \ Als de 1e debet en credit prestatie op \xE9\xE9n moment bekend zijn, dan horen\
  \ die tegen elkaar weg te vallen en niet in \xE9\xE9n bericht te staan.\nEen credit\
  \ prestatie en een 2e debet prestatie kunnen desgewenst in hetzelfde bericht worden\
  \ aangeleverd.\n\nTotaal declaratie-/factuurbedrag\nHet totale declaratie-/factuurbedrag\
  \ van alle debet en credit prestaties in het bericht wordt ingevuld in DeclaratieFactuurTotaalBedrag\
  \ in de header van het bericht. Hierbij worden debetbedragen opgeteld en creditbedragen\
  \ afgetrokken."
titel: 'IV055: Hoe moet worden omgegaan met debet en credit prestaties in een declaratie-
  of factuurbericht?'
type: Invulinstructie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

