---
# python/object:istd_klassen.IstdRegel
code: IV086
documentatie: "Debet en credit prestaties kunnen in 1 declaratiebericht worden aangeleverd.\
  \ Hierbij geldt: \n* Een 1e debet prestatie en een identieke credit prestatie mogen\
  \ niet in 1 declaratiebericht worden aangeleverd. Als de 1e debet en credit prestatie\
  \ op 1 moment bekend zijn, dan horen die tegen elkaar weg te vallen en niet in 1\
  \ bericht te staan.\n* Een credit prestatie en een 2e debet prestatie kunnen desgewenst\
  \ in hetzelfde bericht worden aangeleverd.\n\nTotaal declaratiebedrag \nHet totale\
  \ declaratiebedrag van alle debet en credit prestaties in het declaratiebericht\
  \ wordt ingevuld in TotaalIngediendBedrag in de header van het bericht. Hierbij\
  \ worden debetbedragen opgeteld en creditbedragen afgetrokken. \n\n"
titel: 'IV086: Hoe moet worden omgegaan met debet en credit prestaties in een declaratiebericht?'
type: Invulinstructie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

