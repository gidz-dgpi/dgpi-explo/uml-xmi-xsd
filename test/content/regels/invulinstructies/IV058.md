---
# python/object:istd_klassen.IstdRegel
code: IV058
documentatie: "Bij meerdere identieke producten op \xE9\xE9n dag, waarbij alle overige\
  \ declaratie-/factuur-inhoudelijke gegevens identiek zijn, moet dit in het declaratie-\
  \ of factuurbericht worden samengevoegd tot \xE9\xE9n berichtklasse prestatie (met\
  \ als aantal het desbetreffende aantal). "
titel: "IV058: Hoe moet de prestatie gevuld worden wanneer hetzelfde product meer\
  \ dan \xE9\xE9n keer op een dag aan een client is geleverd?"
type: Invulinstructie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

