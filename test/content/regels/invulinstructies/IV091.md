---
# python/object:istd_klassen.IstdRegel
code: IV091
documentatie: "In de IngediendBedrag-regel van een Prestatie wordt met DebetCredit\
  \ aangegeven of de indienende partij een vordering (debet) of een terugvordering\
  \ (credit) doet bij de ontvanger van de declaratie.\n\nCrediteren declaratie: \n\
  Een creditprestatie is identiek aan de oorspronkelijke debetprestatie, met uitzondering\
  \ van:\n* ReferentieNummer: de creditprestatie krijgt een nieuw ReferentieNummer.\n\
  * VorigReferentieNummer wordt gevuld met het ReferentieNummer van de oorspronkelijke\
  \ debetprestatie.\n* DebetCredit in IngediendBedrag wordt gevuld met C (Credit)\n\
  \nCrediteren is alleen mogelijk na ontvangst van het declaratie-antwoordbericht\
  \ waarin de te crediteren prestatie is toegekend, zie IV085).\n"
titel: 'IV091: Hoe moet een creditprestatie gevuld worden in een declaratiebericht?'
type: Invulinstructie

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

