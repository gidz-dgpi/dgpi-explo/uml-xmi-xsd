---
# python/object:istd_klassen.IstdRegel
code: TR349
controleniveau: berichtoverstijgend
documentatie: "Het verzoek om wijziging bericht bevat alle actuele toegewezen producten\
  \ van de client, hetzij in OngewijzigdProduct, hetzij in TeWijzigenProduct\nActuele\
  \ toewijzingen zijn toewijzingen die op of na de huidige datum geldig zijn, of waarvan\
  \ de ingangsdatum in de toekomst ligt. \nAlle actuele toewijzingen zijn terug te\
  \ vinden, hetzij als OngewijzigdProduct, hetzij als TeWijzigenProduct.\n"
retourcode: '9349'
titel: 'TR349: Het verzoek om wijziging bericht bevat alle actuele ToegewezenProducten
  van de client'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

