---
# python/object:istd_klassen.IstdRegel
code: TR071
controleniveau: berichtoverstijgend
retourcode: '9071'
titel: 'TR071: StatusAanlevering mag niet de waarde ''3'' bevatten als er voor de
  betreffende melding start zorg al een stop zorg is verstuurd.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

