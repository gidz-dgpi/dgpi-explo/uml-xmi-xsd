---
# python/object:istd_klassen.IstdRegel
code: TR340
controleniveau: berichtoverstijgend
documentatie: 'Indien de ProductCode in de Toewijzing gevuld is, dan moet in het declaratiebericht
  in de Prestatie de toegewezen ProductCode overgenomen worden.  '
retourcode: '9340'
titel: 'TR340: ProductCode in Prestatie moet gelijk zijn aan ProductCode in het ToegewezenProduct,
  indien deze opgenomen is.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

