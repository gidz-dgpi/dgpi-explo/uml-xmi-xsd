---
# python/object:istd_klassen.IstdRegel
code: TR363
controleniveau: berichtoverstijgend
documentatie: Elke toewijzing die op basis van het verzoek om wijziging wordt gegeven
  krijgt dezelfde ReferentieAanbieder, die wordt overgenomen uit het verzoek
retourcode: '9363'
titel: 'TR363: Als een toewijzingsbericht wordt gestuurd als honorering van een verzoek
  om wijziging, wordt ReferentieAanbieder overgenomen'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

