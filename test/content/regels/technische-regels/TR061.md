---
# python/object:istd_klassen.IstdRegel
code: TR061
controleniveau: standaard
retourcode: '0001'
titel: "TR061: Bij een Client moet minimaal \xE9\xE9n Adres voorkomen waarvan Soort\
  \ de waarde '01' (BRP-adres), '02' (Correspondentie-adres) of '03' (Verblijfadres)\
  \ heeft."
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

