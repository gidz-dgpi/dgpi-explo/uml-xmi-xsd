---
# python/object:istd_klassen.IstdRegel
code: TR373
controleniveau: berichtoverstijgend
documentatie: Als in een verzoek om wijziging een budgetaanpassing wordt gevraagd,
  is dit altijd voor de gehele looptijd van de betreffende toewijzing.
retourcode: '9373'
titel: 'TR373: Indien in een TeWijzigenProduct het Budget of het totaal over toewijzingsperiode
  wordt gewijzigd, dient GewensteIngangsdatum gelijk te zijn aan de actuele toewijzing'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

