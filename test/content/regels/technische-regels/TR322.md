---
# python/object:istd_klassen.IstdRegel
code: TR322
controleniveau: berichtoverstijgend
documentatie: Hierbij moet rekening worden gehouden met de waarden van Eenheid, Frequentie
  en totale loopduur van het ToegewezenProduct enerzijds en de waarde van Eenheid
  en de duur van de DeclaratiePeriode in de ingediende Prestaties anderzijds. De som
  van het GeleverdVolume in alle ingediende Prestaties die betrekking hebben op dat
  ToegewezenProduct mag het maximale volume berekend over de loopduur van de toewijzing
  niet overstijgen.
retourcode: '9322'
titel: 'TR322: Indien in het ToegewezenProduct een Omvang is meegegeven, moet de som
  van GeleverdVolume in alle ingediende Prestaties die betrekking hebben op dat ToegewezenProduct
  passen binnen de toegewezen Omvang.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

