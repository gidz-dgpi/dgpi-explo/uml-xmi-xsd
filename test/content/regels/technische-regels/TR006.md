---
# python/object:istd_klassen.IstdRegel
code: TR006
controleniveau: standaard
documentatie: '

  '
retourcode: '0001'
titel: 'TR006: De berichtklasse Contact moet een berichtelement Telefoon, en/of de
  combinatie Huis en Postcode bevatten.'
type: Technische regel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

