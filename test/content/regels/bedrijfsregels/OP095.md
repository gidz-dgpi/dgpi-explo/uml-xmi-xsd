---
# python/object:istd_klassen.IstdRegel
code: OP095
titel: 'OP095: Een bericht mag niet worden afgekeurd op basis van informatie waartoe
  de verzendende partij geen toegang heeft.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

