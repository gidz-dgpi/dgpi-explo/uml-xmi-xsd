---
# python/object:istd_klassen.IstdRegel
code: OP076
documentatie: Gegevens over een contactpersoon mogen alleen worden opgenomen indien
  noodzakelijk voor communicatie met de client.
titel: 'OP076: Van iedere contactpersoon (relatie) moet worden opgegeven in welke
  relatie deze tot de client staat.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

