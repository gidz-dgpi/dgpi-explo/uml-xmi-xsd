---
# python/object:istd_klassen.IstdRegel
code: OP033
documentatie: 'Correcties op een melding start product zijn uitsluitend toegestaan
  voor het geleverde product. Als de ondersteuning beeindigd is, kan de aanvang niet
  meer op deze manier gecorrigeerd worden.


  Met de status aanlevering van een berichtklasse kan worden aangegeven of:

  een berichtklasse nieuw is (waarde 1);

  een berichtklasse gewijzigd is (waarde 2);

  een berichtklasse verwijderd moet worden (waarde 3). Een verwijdering betekent dat
  de vorige aanlevering met dezelfde sleutel als niet verzonden beschouwd moet worden.'
titel: 'OP033: Voor het doorgeven van wijzigingen en correcties op een eerder verzonden
  bericht, moet gebruik gemaakt worden van de systematiek van status aanlevering.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

