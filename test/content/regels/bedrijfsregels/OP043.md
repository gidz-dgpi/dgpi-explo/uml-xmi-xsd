---
# python/object:istd_klassen.IstdRegel
code: OP043
documentatie: Een toewijzing eindigt (uiterlijk) op de dag voorafgaand aan de ingangsdatum
  van het PGB.
titel: 'OP043: Indien aan een client een PGB is toegekend is, mag op hetzelfde moment
  voor hetzelfde product geen toewijzing zijn afgegeven.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

