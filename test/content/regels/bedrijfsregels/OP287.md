---
# python/object:istd_klassen.IstdRegel
code: OP287
documentatie: 'Bij een volledige toekenning zal het ingediende bedrag in de som van
  de door de gemeente toegekende bedragen in de header van het factuurretourbericht
  worden meegenomen.

  Bij een volledige afwijzing zal het toegekende bedrag in het factuurretourbericht
  gelijk zijn aan 0 (nul).'
titel: 'OP287: Een door de aanbieder ingediend bedrag behorende bij een prestatie
  op een factuur wordt door de gemeente altijd volledig toegekend of volledig afgewezen.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

