---
# python/object:istd_klassen.IstdRegel
code: OP011
titel: 'OP011: De aanbieder meldt de begindatum van de levering nadat de ondersteuning
  daadwerkelijk is aangevangen.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

