---
# python/object:istd_klassen.IstdRegel
code: OP284
documentatie: 'Als gedurende een volledige declaratie-/factuurperiode door de aanbieder
  geen zorg is geleverd, zal de aanbieder over die betreffende periode geen declaratie/factuur
  indienen. Tenzij in deze declaratie-/factuurperiode correcties op voorgaande declaraties/facturen
  moeten worden verstuurd. Dit betekent dat er hiaten in de opvolgende declaratie-/factuurperioden
  kunnen voorkomen. Het hiaat heeft altijd de duur van 1 of meer keer de afgesproken
  declaratie-/factuurperiode.


  Overlap van declaratie-/factuurperiode is (voor correcties) toegestaan op de laatst
  ingediende declaratie-/factuurperiode. Zie ook IV064 voor declaratie- / facctuurberichten
  (303) en IV090 voor declaratieberichten(323).

  '
titel: 'OP284: Indien iedere declaratie-/factuurperiode zorg is geleverd door de aanbieder,
  moeten de declaratie-/factuurperioden aansluitend zijn in opvolgende declaratie/-factuurberichten.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

