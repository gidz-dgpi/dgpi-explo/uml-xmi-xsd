---
# python/object:istd_klassen.IstdRegel
code: OP277
documentatie: 'Declaratie

  De aanbieder die een declaratieretourbericht ontvangt kan op basis van de meegestuurde
  gegevens de reactie op de declaratie verwerken in haar systeem. Dit is vooral van
  belang wanneer van een ingediend declaratiebericht een deel van de ingediende prestaties
  niet (volledig) wordt toegekend. In het retourbestand worden alle prestaties meegestuurd
  die deels zijn toegekend en alle prestaties die zijn afgewezen. Dit zijn alle prestaties
  waarvoor geldt dat het ingediende bedrag van de prestatie niet overeenkomt met het
  door de gemeente toegekende bedrag. Daarbij wordt met een retourcode aangegeven
  wat de reden is dat de ingediende prestatie niet (volledig) is toegekend.


  Factuur

  De aanbieder die een factuurretourbericht ontvangt kan op basis van de meegestuurde
  gegevens vaststellen welke ingediende debet prestaties door de gemeente zijn toegekend
  (worden niet meegestuurd in het retourbericht) en welke ingediende debet prestaties
  door de gemeente zijn afgewezen (worden meegestuurd in het retourbericht met een
  reden van afkeur). Zie ook IV056.

  Ingediende credit prestaties mogen door de gemeente niet worden afgekeurd, tenzij
  ze technisch niet correct zijn. De ingediende (technisch onjuiste) credit prestatie
  dient in dat geval opnieuw te worden ingediend.

  De gemeente betaalt alleen de ingediende debet prestaties die door de gemeente zijn
  toegekend. De ingediende debet prestaties die door de gemeente zijn afgewezen worden
  door de aanbieder beoordeeld. Indien de aanbieder het eens is met de (reden van)
  afwijzing, zal de aanbieder de afgewezen prestatie crediteren en opnieuw (met de
  juiste informatie) als debet prestatie indienen. Indien de aanbieder het niet eens
  is met de (reden van) afwijzing, zal deze buiten het berichtenverkeer contact opnemen
  met de gemeente om de vervolgstappen af te stemmen.

  N.B.: De som van de toegekende bedragen in de header van het factuurretourbericht
  mag (in tegenstelling tot deze informatie in het declaratieretourbericht) niet worden
  gebruikt bij het administreren van de boekhouding. De boekhouding van de aanbieder
  wordt altijd bijgewerkt op basis van de ingediende facturen. Het factuurretourbericht
  dient wel als basis voor de aanbieder om te weten welke ingediende debet prestaties
  moeten worden gecrediteerd.


  Volledig toegekende prestaties

  Volledig toegekende prestaties worden niet opgenomen in het retourbericht. Alleen
  de som van de bedragen van de toegekende prestaties wordt in de header van het retourbericht
  opgenomen.'
titel: 'OP277: Het factuur- of declaratieretourbericht bevat alle informatie om het
  bericht administratief te verwerken.'
type: Bedrijfsregel

---

**Generereerd door `export_md_content.py` op 13 February, 2023**

