# UML XMI weergeve verkenning

Deze verkenning is bedoeld om te onderzoeken hoe een XMI UML-model export uit StarUML via de HUGO Sitegenerator kan worden gepubliceerd. 

## Uitgangspunten

- Metamodel https://gitlab.com/istd-dgpi/docs/istd-meta-model
- Bericht
  - content-naam = <berictnaam>
  - ...XsdConfiguratie

## HUGO Configuratie

Benodigde additionele configuratie van HUGO.

### HUGO config/_default/params.yml

Locatie XMI UML-model export binnen `assets` folder:

```yaml
iStd:
  UmlModel: models/iWMO.xmi
```

*NB: In bovenstaande geval staat het model dus in folder `assets/models/iWmo.xmi`*

### HUGO config/_default/mediaTypes

XML moet ook via de extensie `XMI` en `XSD` worden herkend.

```yaml
application/xml:
  suffixes:
  - xml
  - xmi
  - xsd
```

## StartUML Configuratie

### StarUML CLI (Command Line Interface)

De **StarUML CLI** kan worden gebruikt om handmatige handelingen te automatiseren.

Zie [Creating Alias](https://docs.staruml.io/user-guide/cli-command-line-interface#creating-alias) (Command Line Interface) configuratie.

zie [CLI exec command](https://docs.staruml.io/user-guide/cli-command-line-interface#exec).

Zie [xmi:import command](https://github.com/staruml/staruml-xmi/blob/da89d9cee5f217a29a443ed844cf20276955c5cf/main.js#L64)


