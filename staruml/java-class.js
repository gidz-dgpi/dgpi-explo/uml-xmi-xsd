const fs = require('fs');
const { exec } = require('node:child_process');
const staruml = '"/Applications/StarUML.app/Contents/MacOS/StarUML"';
const outputDir = 'test-output';
const args = 'ejs assets/models/iWMO.mdj -t staruml/java-class.ejs -s @UMLClass -o "test-output/<%=filenamify(element.name)%>.java"';

console.log('run staruml');
try { fs.mkdirSync(outputDir);} 
catch (error) { 
  console.log(error.code + ' => ' + error.path)
}

exec(staruml + ' ' + args, (error, stdout, stderr) => {
  if (error) {
    console.error(`exec error: ${error}`);
    return;
  }
  console.log(`stdout: ${stdout}`);
  console.error(`stderr: ${stderr}`);
});