# StarUML CLI EJS

Gebruiken van EJS via de StarUML CLI ejs template optie.

Bronnen:
- Documentatie StarUML CLI EJS https://docs.staruml.io/user-guide/cli-command-line-interface#ejs
- Voorbeeld dump Java-Klassen https://github.com/staruml/staruml-cli-examples/blob/main/ejs/java-class.ejs

Testen werking CLI EJS

```ejs
staruml ejs assets/models/iWMO.mdj -t staruml/java-class.ejs -s @UMLClass -o "public/<%=filenamify(element.name)%>.java"
```

> Bovenstaand script werkt


- https://docs.staruml.io/developing-extensions/accessing-elements#type-selector-t
