# iStandaard Meta-Model Mapping

Uitkomsten van de verkenning waarmee is onderzocht:

- Hoe een iStandaard Berichtmodel in StarUML-model via UML-XMI-export kan worden gepubliceerd?

  - Hoe kan een [UML-XMI-export](../assets/models/iWMO.xmi) op het [iStandaard Meta Model](https://gitlab.com/istd-dgpi/docs/istd-meta-model) worden afgebeeld?

  - Hoe kan deze afbeelding op het op het [iStandaard Meta Model](https://gitlab.com/istd-dgpi/docs/istd-meta-model) vervolgens worden gebruik om te komen tot?
    - Een PlantUML-weergave als Berichtdiagram
    - Een XSD-koppelvlakspecificatie voor een Bericht

- Hoe kunnen de Configuratie- en Content-parameters voor aanvullende (niet in een UML-model vast te leggen) gegevens worden gestructureerd?


> Hieronder worden de uitkomsten van de verkenning in detail beschreven vanuit de ontwikkelde `technische componenten` die aan het bouw-team van de *DgPi Leverstraat* zijn overgedragen.

# Weergave Bericht Diagram

Voor der weergave van het bericht wordt op basis van de [iStandaard Bericht Model]() een [PlantUML Classdiagram](https://plantuml.com/class-diagram) weergave generereerd.

De implementatie 

# Mapping Bericht XSD

Een XSD-koppelvlakspecificatie voor een Bericht is een samenstelling van:
- het voor iStandaarden vastgestelde XSD-formaat
- en de samengestelde inhoud uit het Informatiemodel

In onderstaande tabel is de inhoud voor het samenstellen van de XSD's nader beschreven.

| Typering inhoud | Bron(nen) | Toelichting |
|:-|:-|:-|
| Constanten | Vaste waarden in het [Bericht XSD Template](../templates/xsd/bericht.xsd.template) | Deze waarden gelden voor alle headers en schema's in de XSD's van iStandaard-berichten |
| Configuratie | [Configuratie Parameters](../config/_default/params.yml) | Deze waarden  gelden voor een gehele publicatie release van een iStandaard. Bijv mapping-variabele **<%= schema %>** |
| Content | Frontmatter-variabelen| Deze waarden gelden voor een specifiek onderdeel van de publicatie (bericht, regel of codelijst) met de ***<%= variabeleNaam %>*** als placeholder |
| BerichtModel | Waarden vanuit XMI-export van UML-model aangevuld met waarden van Frontmatter-variabelen uit de Content | Definitie van het iStandaard bericht-format in de *UML-standaard* aangevuld met *iStandaard-specifieke Content* met **<%= berichtModel %>** als placeholder. Zie voor details [BerichtModel](#berichtmodel) |

Zie hieronder de afbeelding van de *inhoud* op het *XSD-formaat template* van een iStandaard-bericht

```plantuml
@startmindmap
*[#white] XSD

**[#lightblue] ?xml
***[#lightyellow] version
****_ "1.0"
***[#lightyellow] encoding
****_ "UTF-8"

**[#lightblue] xs:schema
***[#lightyellow] xmlns:xs
****_ "**<%= schema %>**"
***[#lightyellow] xmlns:**<%= standaard %>**
****_ "**<%= nameSpace %>/<%= standaard %>**/**<%= releaseMajor %>**_**<%= releaseMinor %>**/basisschema/schema"
***[#lightyellow] xmlns:**<%= berichtNaam %>**
****_ "**<%= nameSpace %>/<%= standaard %>**/**<%= releaseMajor %>**_**<%= releaseMinor %>**/**<%= berichtNaam %>**/schema"
***[#lightyellow] targetNamespace
****_ "**<%= nameSpace %>**/**<%= standaard %>**/**<%= releaseMajor %>**_**<%= releaseMinor %>**/**<%= berichtNaam %>**/schema"
***[#lightyellow] elementFormDefault
****_ "qualified"

***[#lightblue] xs:import
****[#lightyellow] namespace
*****_ "**<%= nameSpace %>**/**<%= standaard %>**/**<%= releaseMajor %>**_**<%= releaseMinor %>**/basisschema/schema"
****[#lightyellow] schemaLocation
*****_ "**<%= basisSchemaXsdLocation %>**" 

***[#lightblue] xs:annotation
****[#lightblue] xs:appinfo
*****[#lightblue] **<%= standaard %>**:standaard
******[#lightyellow] **<%= standaard %>**
*****[#lightblue] **<%= standaard %>**:bericht
******[#lightyellow] **<%= berichtNaam %>**
*****[#lightblue] **<%= standaard %>**:release
******[#lightyellow] **<%= releaseMajor %>**.**<%= releaseMinor %>**
*****[#lightblue] **<%= standaard %>**:BerichtXsdVersie
******[#lightyellow] **<%= versieBerichtXsd %>**
*****[#lightblue] **<%= standaard %>**:BerichtXsdMinVersie
******[#lightyellow] **<%= minVersieBerichtXsd %>**
*****[#lightblue] **<%= standaard %>**:BerichtXsdMaxVersie
******[#lightyellow] **<%= maxVersieBerichtXsd %>**
*****[#lightblue] **<%= standaard %>**:BasisschemaXsdVersie
******[#lightyellow] **<%= versieBasisschemaXsd %>**
*****[#lightblue] **<%= standaard %>**:BasisschemaXsdMinVersie
******[#lightyellow] **<%= minVersieBasisschemaXsd %>**
*****[#lightblue] **<%= standaard %>**:BasisschemaXsdMaxVersie
******[#lightyellow] **<%= maxVersieBasisschemaXsd %>**

***[#white] **<%= berichtModel %>**
@endmindmap
```

| Inhoud | Bron-bestand(en) | Bron-waarde | Metamodel-referentie |
|:-|:-|:-|:-|
| "..." | [Bericht XSD Template](../templates/xsd/bericht.xsd.template) | *Constante* |  |
| **<%= schema %>** | [Configuratie Parameters](../config/_default/params.yml) | *iStd.schema* |  |
| **<%= standaard %>** | [Configuratie Parameters](../config/_default/params.yml) | *iStd.standaard* | [IstdStandaard](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Model.md?ref_type=heads#istdstandaard) |
| **<%= releaseMajor %>** | [Configuratie Parameters](../config/_default/params.yml) | *iStd.releaseNummer* (1e deel voor ".") | [IstdRelease](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Model.md?ref_type=heads#istdstandaard) |
| **<%= releaseMinor %>** | [Configuratie Parameters](../config/_default/params.yml) | *iStd.releaseNummer* (2e deel na ".") |  [IstdRelease](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Model.md?ref_type=heads#istdstandaard) |
| **<%= berichtNaam %>** | content/berichten/`<berichtNaam>`.md<br> | iStandaard benaming voor bericht is de *naam* van het bestand zonder de *extensie* (bijv. uit content-bestand *wmo301.md* volgt de berichtNaam *wmo301*) | [IstdBericht](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Bericht.md?ref_type=heads#istdbericht) |
| **<%= versieBerichtXsd %>** | content/berichten/`<berichtNaam>`.md<br> | Frontmatter-variabele *xsdConfiguratieBericht.versieBerichtXsd* | [IstdBerichtXsdConfiguratie](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Bericht.md?ref_type=heads#istdberichtxsdconfiguratie) |
 **<%= minVersieBerichtXsd %>** | content/berichten/`<berichtNaam>`.md<br> | Frontmatter-variabele *xsdConfiguratieBericht.minVersieBerichtXsd* | [IstdBerichtXsdConfiguratie](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Bericht.md?ref_type=heads#istdberichtxsdconfiguratie) |
 **<%= maxVersieBerichtXsd %>** | content/berichten/`<berichtNaam>`.md<br> | Frontmatter-variabele *xsdConfiguratieBericht.maxVersieBerichtXsd* | [IstdBerichtXsdConfiguratie](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Bericht.md?ref_type=heads#istdberichtxsdconfiguratie) |
| **<%= versieBasisschemaXsd %>** | content/berichten/`<berichtNaam>`.md<br> | Frontmatter-variabele *xsdConfiguratieBericht.versieBasisschemaXsd* | [IstdBasisschemaXsdConfiguratie](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/18-conventie-correctie/Model.md#istdbasisschemaxsdconfiguratie) |
 **<%= minVersieBasisschemaXsd %>** | content/berichten/`<berichtNaam>`.md<br> | Frontmatter-variabele *xsdConfiguratieBericht.minVersieBasisschemaXsd* | [IstdBasisschemaXsdConfiguratie](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/18-conventie-correctie/Model.md#istdbasisschemaxsdconfiguratie) |
 **<%= maxVersieBasisschemaXsd %>** | content/berichten/`<berichtNaam>`.md<br> | Frontmatter-variabele *xsdConfiguratieBericht.maxVersieBasisschemaXsd* | [IstdBasisschemaXsdConfiguratie](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/18-conventie-correctie/Model.md#istdbasisschemaxsdconfiguratie) |
| **<%= berichtModel %>** | Zie [Bericht Model XSD](#mapping-bericht-model-xsd) | Samengestelde mapping van het XSD-formaat voor het bericht | [Bericht](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Bericht.md?ref_type=heads) |

## Implementatie Bericht XSD-generatie

In onderstaande tabel staan de technisch componenten waarmee een Bericht XSD wordt gegenereerd.

| Implementatie onderdeel | Toelichting functie | Opmerkingen |
|:-|:-|:-|
| [Bericht-XSD Template](../templates/xsd/bericht.xsd.template) | Basis template met *<%= placeHolders %>* voor het *parsen* van de inhoud in XSD-formaat | Deze is bij gebrek aan *sluitende* documentatie samengesteld op basis van voorbeelden uit bestaande berichten (reverse engineering) |
| [Bericht-XSD Layout](../layouts/partials/xsd/xsd-bericht.html) | [HUGO-Layout Partial](https://gohugo.io/templates/partials) waarmee de inhoud vanuit het Berichtmodel en Content wordt geparst in het [Bericht-XSD Template](../templates/xsd/bericht.xsd.template) | Dit is een volledig herbruikbare implementatie op basis van de [HUGO Sitegenerator](https://gohugo.io) |

> Deze onderdelen zijn voor beheer en verdere ontwikkeling aan het bouwteam van de *DgPi Leverstraat* overgedragen

# Mapping Bericht Model XSD

Het Berichtmodel is de inhoudelijke basis van de koppelvlakspecificatie van een Bericht XSD en is opgebouwd uit meerdere Berichtklassen waarvan:
- De `Root` en `Header` voor de identificatie van het Bericht.
- En overige Berichtklassen (1:n) voor de *te verzenden/ontvangen inhoud*.

```plantuml
@startmindmap
*[#white] Bericht Model
** Root Klasse
** Header Klasse
**[#white] Bericht Klasse(1)
**[#white] Bericht Klasse(...)
**[#white] Bericht Klasse(n)
@endmindmap
```

# Mapping Bericht Klasse XSD

De samenstelling van  Bericht Klasse XSD wordt afgeleid uit de [StarUML-inrichting berichtmodellen](./ontwerp-keuzes-istandaard-estafette.md#staruml-inrichting-berichtmodellen) en de aanvullende Content voor [Regels](./ontwerp-keuzes-istandaard-estafette.md#regels) en [Codelijsten](./ontwerp-keuzes-istandaard-estafette.md#codelijsten). 

Zie hieronder de afbeelding van de *inhoud* op het *XSD-formaat template* van een iStandaard Berichtklasse.

```plantuml
@startmindmap
*[#white] Berichtklasse
**[#lightblue] xs:complexType
***[#lightyellow] name
****_ "**<%= klasseNaam %>**"
***[#white] Klasse Beschrijving
***[#white] Klasse Elementen
***[#white] Relatie Elementen
**[#white] Bericht Relatieklassen
@endmindmap
```

| Inhoud | Bron-bestand(en) | Bron-waarde | Metamodel-referentie |
|:-|:-|:-|:-|
| **<%= klasseNaam %>** | [UML-XMI-export](../assets/models/iWMO.xmi) | StarUML-model `Berichten/<Bericht>/<Klasse>.name` | [IstdBerichtKlasse](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Bericht.md?ref_type=heads#istdberichtklasse) |


## Implementatie Bericht Klasse XSD-genereratie

In onderstaande tabel staan de technisch componenten waarmee een Berichtklasse XSD wordt gegenereerd.

| Implementatie onderdeel | Toelichting functie | Opmerkingen |
|:-|:-|:-|
| [Bericht Klasse XSD Template](../templates/xsd/bericht-klasse.xsd.template) | Basis template met *<%= placeHolders %>* voor het *parsen* van de inhoud in XSD-formaat | Deze is bij gebrek aan *sluitende* documentatie samengesteld op basis van voorbeelden uit bestaande berichten (reverse engineering) |
| [XSD Bericht Klasse Layout](../layouts/partials/xsd/xsd-bericht-klasse.html) | [HUGO-Layout Partial](https://gohugo.io/templates/partials) waarmee de inhoud vanuit het Berichtklasse en Content wordt geparst in het [Berichtklasse-XSD Template](../templates/xsd/bericht-klasse.xsd.template) | Dit is een volledig herbruikbare implementatie op basis van de [HUGO Sitegenerator](https://gohugo.io) |

> Deze onderdelen zijn voor beheer en verdere ontwikkeling aan het bouwteam van de **DgPi Leverstraat** overgedragen

# Mapping Klasse Beschrijving

Een Klasse Beschrijving moet worden gegenereerd wanneer de *documentatie* voor de UML-klasse in het UML-model is ingevuld.

| Inhoud | Bron-bestand(en) | Bron-waarde | Metamodel-referentie |
|:-|:-|:-|:-|
| **<%= klasseBeschrijving %>** | [UML-XMI-export](../assets/models/iWMO.xmi) | StarUML-model `Berichten/<Bericht>/<Klasse>.documentation` | [IstdBerichtKlasse](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Bericht.md?ref_type=heads#istdberichtklasse) |

## Implementatie Klasse Beschrijving XSD-genereratie

In onderstaande tabel staan de technisch componenten waarmee een Klasse Beschrijving binnen de XSD wordt gegenereerd.

| Implementatie onderdeel | Toelichting functie | Opmerkingen |
|:-|:-|:-|
| [Bericht Klasse XSD Template](../templates/xsd/bericht-klasse.xsd.template) | Basis template met **<%= klasseBeschrijving %>** als *placeholder* voor het *parsen* van de *Berichtklasse Beschrijving* in XSD-formaat | Deze is bij gebrek aan *sluitende* documentatie samengesteld op basis van voorbeelden uit bestaande berichten (reverse engineering) |
| [XS Annotation Documentation Layout](../layouts/partials/xsd/xs-annotation-documentation.html) | [HUGO-Layout Partial](https://gohugo.io/templates/partials) waarmee de inhoud van de *Klasse Beschrijving* wordt geparst in het [Berichtklasse-XSD Template](../templates/xsd/bericht-klasse.xsd.template) | Dit is een volledig herbruikbare implementatie op basis van de [HUGO Sitegenerator](https://gohugo.io) |

> Deze onderdelen zijn voor beheer en verdere ontwikkeling aan het bouwteam van de **DgPi Leverstraat** overgedragen

# Mapping Klasse en/of Relatie Elementen

Klasse Elementen worden worden op basis van UML-attributen in een Klasse gegenereerd.

| Inhoud | Bron-bestand(en) | Bron-waarde | Metamodel-referentie |
|:-|:-|:-|:-|
| **<%= klasseElementen %>** | [UML-XMI-export](../assets/models/iWMO.xmi) | StarUML-model `Berichten/<Bericht>/<Klasse>/<Element>` | [IstdBerichtKlasseElement](https://gitlab.com/istd-dgpi/docs/istd-meta-model/-/blob/main/Bericht.md?ref_type=heads#istdberichtklasseelement) |

## Implementatie Klasse Elementen XSD-generatie

In onderstaande tabel staan de tecnische componenten waarmee Klasse Elementen en/of Relatie Elementen binnen de XSD worden gegenereerd.

| Implementatie onderdeel | Toelichting functie | Opmerkingen |
|:-|:-|:-|
| [Klasse Elementen XSD Template](../templates/xsd/klasse-elementen.xsd.template) | Basis template met **<%= klasseElementen %>** en **<%= relatieElementen %>** als *placeholders* voor het *parsen* van de *Klasse Elementen* respectievelijk *Relatie Elementen* in XSD-formaat | Deze is bij gebrek aan *sluitende* documentatie samengesteld op basis van voorbeelden uit bestaande berichten (reverse engineering) |
| [XSD Bericht Klasse Elementen Layout](../layouts/partials/xsd/xsd-bericht-klasse-elementen.html) | [HUGO-Layout Partial](https://gohugo.io/templates/partials) waarmee de inhoud van de *Klasse Elementen en/of Relatie Elementen* worden geparst in het [Klasse Elementen XSD Template](../templates/xsd/klasse-elementen.xsd.template) | Dit is een volledig herbruikbare implementatie op basis van de [HUGO Sitegenerator](https://gohugo.io) |

### klasse

# Basisschema

De XSD-koppelvlakspecificatie voor het Basisschema bestaat uit:
- het voor iStandaarden vastgestelde XSD-formaat voor het Basisschema;
- en het Gegevensmodel dat als basis voor de Berichten wordt gebruikt.

