---
weight: 1
---
# Ontwerp-keuzes DgPi iStandaard Estafette

Ontwerpkeuzes voor de ontvlechting van de huidige iStandaard Estafette modellen uit BizzDesign.

## Generieke keuzes

De generieke keuzes gaan over de vraag hoe gegevens worden beheerd. Daarbij zijn voor **DgPi** de volgende keuzes te maken:

> **Als Model** = via een (grafische) weergave de inhoud en relaties van meta-modellen beheren volgens *open standaarden* met *StarUML* als (in de verkenning geevalueerde) modelleertool.

> **Als Content** = via (semi-gestructureerd) tekst de inhoud en variabelen de meta-data beheren. Daarbij gebruik maken van de *open standaarden* voor *Markdown* (vrije tekst) en *Frontmatter* (vaste variabelen). Bijvoorbeeld menu-structuuur, toelichtende tekst, tabelgegevens. Eventueel nog nader te verkennen is het mogelijk gebruik van een [Headless CMS](https://www.netlify.com/blog/complete-guide-to-headless-cms/) in plaats van een (ruwe) Tekst-editor.

Hieronder staan de generieke ontwerpvragen met de daarvoor gemaakte keuzes samengevat

| Ontwerpvraag |Keuze |
|:-|:-|
| Hoe gaan we de Vorm en Inhoud van de publicatie beheren? | Als Content |
| Hoe gaan we de Berichtmodellen en Gegevens beheren? | Als Model |
| Hoe gaan we de Regels beheren? | Als Content |
| Hoe gaan we de Codelijsten beheren? | Als Content |

Hieronder wordt per functioneel onderdeel ingegaan op de gegevensbeheer-aspecten vanuit de uitgevoerde verkenningen. De publicatie-aspecten worden in de [iStandaard Meta-Model Mapping](istd-meta-model-mapping.md) beschreven.


## Berichtmodellen

De modellen voor de inhoud van de iStandaard Estafette `Berichten` worden als UML-diagrammen beheerd.

### Standaardisatie berichtmodellen

Met onderstaande keuzes wordt voldaan aan het toepassen van de huidige *UML-diagram* standaarden en het kunnen uitwisselen van *meta-data* voor UML-diagrammen.

| Onderdeel | Keuze | Toelichting |
|:-|:-|:-|
| Beheer | StarUML | Gebruik UML-modelstandaard |
| Data | XMI | Gebruik UML-uitwisselingsstandaard |

### StarUML-inrichting berichtmodellen

Zie hieronder hoe in de StarUML-modelleertool de Berichten worden ingedeeld.

| Model| Package | Klasse | Eigenschap | Toelichting |
|:-|:-|:-|:-|:-|
| `Berichten` | | | | Vaste naam |
| | `<Bericht>` | | | Bericht bijv. `WMO301` |
| | | `<Klasse>` | | Bericht-klasse bijv. `Header`|
| | | | `<Element>` | Bericht-klasse-element bijv. `Berichtcode`|


*Voorbeeld: Berichtmodelstructuur in StarUML*

![](dgpi-staruml-berichtmodel-structuur.png)

*Voorbeeld: Berichtdiagram in StarUML*

![](dgpi-staruml-berichtdiagram.png)


## Gegevensmodel

Aangezien de gegevens de *bouwblokken* zijn voor de *berichtmodellen* worden daarvoor vergelijkbare keuzes voor het modelleren  gehanteerd.

### Standaardisatie gegevensmodel

Met onderstaande keuzes wordt voldaan aan het toepassen van de huidige *UML Data Type* standaard en het kunnen uitwisselen van *meta-data* voor een UML Data Type.

| Onderdeel | Keuze | Toelichting |
|:-|:-|:-|
| Beheer | StartUML | Gebruik UML-modelstandaard |
| Data | XMI | Gebruik UML-uitwisselingsstandaard |

### StartUML-inrichting gegevensmodel

Zie hieronder hoe in de StarUML-modelleertool de Gegevens worden ingedeeld.

| Package | Data Type | Toelichting |
|:-|:-|:-|
| `Berichten` | | vaste naam |
| | `<dataType>` | Data Type bijv. `LDT_BerichtSubversie` |

*Voorbeeld: Gegevensstructuur in StarUML*

![](dgpi-staruml-gegevens-structuur.png)

## Regels

De regels zijn in de huidige opzet specifiek voor *iStandaarden* gedefinieerd (zonder gebruik van standaarden).

### Standaardisatie regels

Vanuit *standaardisatie* gezien zijn regels van het type *referentiegegevens*.

| Onderdeel | Keuze | Toelichting |
|:-|:-|:-|
| Beheer |  Tekst-editor of Headless CMS | Tooling voor beheer van semi-gestructureerde tekst (korte termijn) |
| | RIGA | Regelbeheer-tooling (lange termijn) |
| Data | *Markdown* en *Frontmatter* | Gebruik open standaard voor semi-gestructureerde gegevens |

### Repository inrichting regels

De *regels* staan onder de *content-directorie* in de *regels-directorie*.

Zie hieronder de indeling van de onderligende *regeltype-directories* en de naamgeving van de *regel-bestanden*.

| Directorie | Bestand(en) | Toelichting |
|:-|:-|:-|
| `uitgangspunten` | | Verzameling uitgangspunten |
| | `_index.md` | Overzicht uitgangspunten |
| | `UP<nnn>.md` | Content uitgangspunt bijv. `UP001.md` |
| `bedrijfsregels` | | Verzameling bedrijfsregels |
| | `_index.md` | Overzicht bedrijfsregels |
| | `OP<nnn>.md` | Content bedrijfsregel bijv. `OP090.md` |
| | `OP<nnn>x<n>.md` | Content extensie op bedrijfsregel bijv. `OP090x2.md` |
| `technische-regels` | | Verzameling technische regels |
| | `_index.md` | Overzicht technische regels |
| | `TR<nnn>.md` | Content technische regel bijv. `TR002.md` |
| `condities` | | Verzameling condities |
| | `_index.md` | Overzicht condities |
| | `CD<nnn>.md` | Content conditie bijv. `CD005.md` |
| `constraints` | | Verzameling constraints |
| | `_index.md` | Overzicht constraints |
| | `CS<nnn>.md` | Content constraint bijv. `CD005.md` |
| | `RS<nnn>.md` | Content beperking bijv. `RS001.md` |
| `invulinstructies` | | Verzameling invulinstructies |
| | `_index.md` | Overzicht invulinstructies |
| | `IV<nnn>.md` | Content invulinstructie bijv. `IV002.md` |

## Codelijsten

### Standaardisatie codelijsten

Vanuit *standaardisatie* gezien zijn codelijsten van het type *referentiegegevens*.

| Onderdeel | Keuze | Toelichting |
|:-|:-|:-|
| Beheer |  Tekst-editor of Headless CMS | Tooling voor beheer van semi-gestructureerde tekst |
| Data | *Markdown* en *Frontmatter* | Gebruik open standaard voor semi-gestructureerde gegevens |

### Repository inrichting codelijsten

De *codelijsten* staan onder de *content-directorie* in de *codelijsten-directorie*.

| Bestand(en) | Toelichting |
|:-|:-|
| `<AAA><nnn>.md` | Lijst met kruisverwijzingen voor een bepaald type codering. Bijv. `COD032.md` voor landcodes. |
